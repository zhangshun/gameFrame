/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.NettyEngine3.core;


import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.handler.traffic.ChannelTrafficShapingHandler;
import org.jboss.netty.handler.traffic.GlobalTrafficShapingHandler;
import org.jboss.netty.handler.traffic.TrafficCounter;
import org.jboss.netty.util.HashedWheelTimer;
import org.jboss.netty.util.Timer;

import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author :陈磊 <br/>
 *         Project:CreazyGameServer1.2
 *         Date: 13-3-17
 *         Time: 上午11:22
 *         connectMethod:13638363871@163.com<br/>
 *
 *         netty 底层处理工具类
 */
public class NServerFrameUtil{

    public static final Timer timer = new HashedWheelTimer(); //计时器（底层读写数据是否超时）,timer must be shared
    /**游戏系统流量监控**/
    public static final GlobalTrafficShapingHandler globalTrafficShapingHandler=new GlobalTrafficShapingHandler(timer, 2);

    /**记录服务器在线人数*/
    public static final AtomicInteger countConnection=new AtomicInteger(0);

    /**
     * 返回ChannelTrafficShapingHandler 对象  per to per
     * @return
     */
    public  static ChannelTrafficShapingHandler getChannelTrafficShapingHandler(){
//        return  new ChannelTrafficShapingHandler(timer, NServerConfig.checkInterval);
        return null;
    }

    /**
     * all
     *启动流量监控器
     */
    public static void startMonitor(){
        globalTrafficShapingHandler.getTrafficCounter().start();
    }

    /**
     * all
     * 停止流量监控器
     */
    public static void stopMonitor(){
        globalTrafficShapingHandler.getTrafficCounter().stop();
    }
    /**
     * all
     * 重置流量统计周期
     * @param newcheckInterval
     */
    public static void configure(long newcheckInterval){

        globalTrafficShapingHandler.getTrafficCounter().configure(newcheckInterval);
    }
    /**
     *  all
     *  返回在interval周期中读取流量的统计
     *  @return the Read Throughput in KB/s computes in the last check interval
     */
    public static long ReadThroughput(){
       return  (globalTrafficShapingHandler.getTrafficCounter().getLastReadThroughput()>>10);
    }
    /**
     *  all
     *  返回在interval周期中读取流量的统计
     *  @return the Read Throughput in KB/s computes in the last check interval
     */
    public static long WriteThroughput(){
        return  (globalTrafficShapingHandler.getTrafficCounter().getLastWriteThroughput()>>10);
    }
    /**
     * 单个连接 流量统计
     * @param trafficCounter
     * @return KB/s
     */
    public static long ReadThroughputPer(TrafficCounter trafficCounter){
        return (trafficCounter.getLastReadThroughput()>>10);
    }
    /**
     *单个连接 流量统计
     * @param trafficCounter
     * @return  KB/s
     */
    public static long WriteThroughputPer(TrafficCounter trafficCounter){
        return (trafficCounter.getLastWriteThroughput()>>10);
    }
    /**
     * 清理  ChannelPipeline资源
     * @param pipeline  pipeline
     */
    public static void clearPipeline(ChannelPipeline pipeline){
        if(null == pipeline){
            return;
        }
        try{
            while(pipeline.getLast()!= null){
                pipeline.removeLast();
            }
        }catch (NoSuchElementException e){
            // all elements removed.
        }
    }

    public static void main(String[]args){

        System.out.println(">>>"+(10000>>10));

    }
}
