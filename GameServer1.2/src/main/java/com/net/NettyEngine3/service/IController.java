/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.NettyEngine3.service;


import com.dc.gameserver.ServerCore.Service.character.PlayerInstance;
import org.jboss.netty.buffer.ChannelBuffer;

import java.io.Serializable;


public interface IController extends Serializable {

    /**一级数据包类型高频游戏数据包**/
    public static final IController[] NEGATIVE_GAME_CONTROLLERS=new IController[128];
    /**二级数据包类型**/
    public static final IController[] GAME_CONTROLLERS=new IController[512];


    /**
     * 数据包分发
     * @param buffer
     * @param player
     */
    void  DispatchPack(ChannelBuffer buffer, PlayerInstance player) throws Exception;

    /**
     * read a byte (1 code)
     * @param buffer buffer
     * @return byte
     */
    byte readByte(ChannelBuffer buffer);
    /**
     *  read a short (2 code)
     * @param buffer  buffer
     * @return short
     */
    short readShort(ChannelBuffer buffer);
    /**
     *  read a short (4 code)
     * @param buffer  buffer
     * @return int
     */
    int  readInt(ChannelBuffer buffer);


    /**
     * @param buffer 待处理数据缓存区
     * @return message
     *       如果默认读取1字节(readBytes())长度的字符串，那么该方法传入buffer将直接返回相应的字符串
     *       编码格式:utf-8
     */
    String readStringUTF8(ChannelBuffer buffer);
    /**
     *
     * @param buffer  待处理数据缓存区
     * @param length 读取指定的数据长度  readBytes() ,readShort() or readLong();
     * @return message
     *         编码格式:utf-8
     */
    String readStringUTF8(ChannelBuffer buffer, int length);

    /**
     *
     * @param buffer   待处理数据缓存区
     * @return  message
     *         如果默认读取1字节长度的字符串，那么该方法传入buffer将直接返回相应的字符串
     *         编码格式:utf-8
     */
    String readString2(ChannelBuffer buffer);

    /**
     * 将buffer数据转化为String
     * 此函数和readString(ChannelBuffer buffer, int length）功能一样的
     * 数据长，注意要跳过length
     * @param buffer  待处理数据缓存区
     * @param length   数据长度
     * @return  message
     */
    String readString2(ChannelBuffer buffer, int length);

    /**
     * 以gbk编码解析数据string
     * @param buffer
     * @param length
     * @return
     */
    String readStringGBK(ChannelBuffer buffer, int length);


    /**
     *  构建数据包 ，处理一级类型数据包
     * 注意：此方法适应的协议是：
     *       paras数据结构-----> length+msg+length+msg ... ...
     *       length: byte(1字节),short（2字节）,int（4字节）
     * @param arg1 类型1  注意 ：args1<0
     * @return  buffer
     */
    ChannelBuffer getWriteBufferA(int arg1, Object... paras);

    /**
     * 构建数据包 ，处理二级类型数据包
     * @param arg1 类型1
     * @param arg2  类型2
     * @param paras 待发送的数据
     *              paras数据-----> length+msg+length+msg ... ...
     *              length: byte(1字节),short（2字节）,int（4字节）
     * @return buffer
     */
    ChannelBuffer getWriteBufferB(int arg1, int arg2, Object... paras);

    /**
     * 解析buffer数据为json对象
     * @param buffer
     * @param clazz
     * @return
     */
    Object parseObject(ChannelBuffer buffer, Class clazz);
    /**
     * 回收buffer
     * @param buffer
     */
    void rebackBuf(ChannelBuffer buffer);

    /**
     * 是否注册该数据包（只针对数据发:client---->server）
     * @return
     */
    boolean AutoMapper();
}
