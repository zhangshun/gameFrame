/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.MinaEngine.core;


import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

/**
 * @author :石头哥哥<br/>
 *         Project:FrameServer1.0
 *         Date: 13-5-19
 *         Time: 下午4:30
 *
 *        mina filter like netty pipeline ,factroy will be creat
 *        codec;
 */
public class MServerProtocolCodecFactory implements ProtocolCodecFactory {


    private final MServerProtocolDecoder decoder;
    private final MServerProtocolEncoder encoder;


    public MServerProtocolCodecFactory(){
        this.decoder=new MServerProtocolDecoder();
        this.encoder=new MServerProtocolEncoder();
    }

    /**
     * Returns a new (or reusable) instance of {@link ProtocolDecoder} which
     * decodes binary or protocol-specific data into message objects.
     */
    @Override
    public ProtocolDecoder getDecoder(IoSession session) throws Exception {
        return this.decoder;
    }

    /**
     * Returns a new (or reusable) instance of {@link ProtocolEncoder} which
     * encodes message objects into binary or protocol-specific data.
     */
    @Override
    public ProtocolEncoder getEncoder(IoSession session) throws Exception {
        return  this.encoder ;
    }



}
