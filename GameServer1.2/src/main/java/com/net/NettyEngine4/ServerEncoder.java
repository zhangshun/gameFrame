/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.NettyEngine4;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;

/**
 * Created with IntelliJ IDEA.
 * User: 石头哥哥  </br>
 * Date: 13-10-22            </br>
 * Time: 下午4:55              </br>
 * Package_name: Server.ExtComponents.NettyEngine4   </br>
 * 注意encoder编码byteBuffer类型                     </br>
 * 编码在构建数据包完成 （逻辑层处理完毕）     </br>
 */
@Deprecated
public class ServerEncoder extends ChannelOutboundHandlerAdapter {

    /**
     * 注意 如果在业务层中编码 ok 这里的编码器可以去掉
     *
     * @param ctx
     * @param msg
     * @param promise
     * @throws Exception
     */
    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {

        /**
         * 逻辑编码
         ByteBuf byteBuf = PooledByteBufAllocator.DEFAULT.heapBuffer();
         //处理编码
         byteBuf.writeBytes((ByteBuf) msg);
         ctx.writeAndFlush(byteBuf, promise);
         */

    }
}
