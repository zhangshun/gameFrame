/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;

import java.io.InputStream;
import java.util.*;

/**
 * @auther:陈磊 <br/>
 * Date: 12-12-10<br/>
 * Time: 下午1:54<br/>
 * connectMethod:13638363871@163.com<br/>
 */
public class PropertyStringUtil {
    private final static String EQUALS_SYMBOL = "="; // 等于号
    private final static String SPLIT_SYMBOL = "|"; // 分隔符
    private final static String SPLIT_REGEX_SYMBOL = "\\|"; // //分隔符正则表达式

    public static String map2String(Map<String, ?> attributes) {
        if (attributes == null) {
            return "";
        } else {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, ?> entry : attributes.entrySet()) {
                String key = entry.getKey().toString().trim();
                String value = entry.getValue().toString();
                builder.append(key + EQUALS_SYMBOL + value + SPLIT_SYMBOL);
            }
            return builder.toString();
        }
    }

    public static String property2String(Properties attributes) {
        if (attributes == null) {
            return "";
        } else {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<Object, Object> entry : attributes.entrySet()) {
                String key = entry.getKey().toString().trim();
                String value = entry.getValue().toString();
                builder.append(key + EQUALS_SYMBOL + value + SPLIT_SYMBOL);
            }
            return builder.toString();
        }
    }

    public static Properties string2Property(String attributes) {
        Properties attributeProperties = new Properties();
        if (attributes != null) {
            String[] splitAttribute = attributes.split(SPLIT_REGEX_SYMBOL);
            for (String a : splitAttribute) {
                if (a != null) {
                    String[] kv = a.split(EQUALS_SYMBOL);
                    if (kv.length == 2) {
                        attributeProperties.setProperty(kv[0], kv[1]);
                    }
                }
            }
        }
        return attributeProperties;
    }

    public static Properties loadProperties(String path) throws Exception {
        Properties p = new Properties();
        InputStream is = PropertyStringUtil.class.getResourceAsStream(path);
        p.load(is);
        if (is != null) {
            is.close();
        }
        return p;
    }

    public static Object[] buildOptions(String attributes) {
        List<Map<String, String>> options = new ArrayList<Map<String, String>>();

        Map<String, String> e = null;
        if (attributes != null && attributes.trim().length() > 0) {
            String[] opts = attributes.split("\\|");
            for (String v : opts) {
                String[] arrStr = v.split("=");
                if (arrStr.length == 2) {
                    e = new HashMap<String, String>();
                    e.put(arrStr[0], arrStr[1]);
                    options.add(e);
                }
            }
        }

        return options.toArray();
    }

    public static void main(String[] args) {
        String value = "|a=123|b=aaaa|A=bbb|aaa=bbb=ccc||";
        //Properties p = string2Property(value);
        for(Object m : buildOptions(value)){
            System.out.println(m);
        }
//		System.out.println(buildOptions(value));
//		value = property2String(p);
        System.out.println(value);

        // String path = "/task/explist.properties";
        // Properties p = loadProperties(path);
        // System.out.println(p.get("rw001001"));
    }
}
