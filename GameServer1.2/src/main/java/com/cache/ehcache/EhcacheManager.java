/*
 * Copyright (c) 2015.
 * 游戏服务器核心代码编写人石头哥哥拥有使用权
 * 最终使用解释权归创心科技所有
 * 联系方式：E-mail:13638363871@163.com ;
 * 个人博客主页：http://my.oschina.net/chenleijava
 * powered by 石头哥哥
 */

package com.cache.ehcache;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author 石头哥哥
 *         </P>
 *         Date:   2015/5/28
 *         </P>
 *         Time:   10:51
 *         </P>
 *         Package: dcServer-parent
 *         </P>
 *         <p/>
 *         注解：    ehcachemanager
 *         spring inject ehcachemanager ,defalut is singleton
 */
@Service
public class EhcacheManager {

    /**
     * cache manager in ehcache
     * singleton CacheManager
     */
    private final CacheManager cacheManager;

    public void setEhcaheXML(String ehcaheXML) {
        this.ehcaheXML = ehcaheXML;
    }

    private String ehcaheXML = "res/gameConfig/ehcache.xml";


    /**
     *
     * @param ehcaheXML
     */
    private EhcacheManager(String ehcaheXML) {
        this.ehcaheXML = ehcaheXML;
        this.cacheManager = new CacheManager(ehcaheXML);
    }

    /**
     *
     */
    private EhcacheManager() {
        this.cacheManager = new CacheManager(ehcaheXML);
    }


    /**
     * @return
     */
    @Deprecated
    public static EhcacheManager getInstance() {
        return SingletonHolder._instance;
    }


    /**
     * cacheName 查找出cache name中 所有对象
     *
     * @param cacheName
     * @return
     */
    public ArrayList<Object> getList(String cacheName) {
        try {
            Cache cache = this.cacheManager.getCache(cacheName);
            if (cache == null) {
                throw new RuntimeException("please check ehcache.xml,there's not " +
                        cacheName + " cache partition or remove it?!");
            }
            Collection<Element> elementCollection = cache.getAll(cache.getKeys()).values();
            if (elementCollection.size() == 0) return null;
            ArrayList<Object> objectArrayList = new ArrayList<Object>();
            for (Element element : elementCollection) {
                objectArrayList.add(element.getObjectValue());
            }
            return objectArrayList;
        } catch (NullPointerException e) {
            return null;                   //
        }
    }


    /**
     * @param cacheName
     * @param key
     * @return
     */
    public Object get(String cacheName, final Object key) {
        try {
            Cache cache = this.cacheManager.getCache(cacheName);
            if (cache == null) {
                throw new RuntimeException("please check ehcache.xml,there's not " +
                        cacheName + " cache partition or remove it?!");
            }
            return cache.get(key).getObjectValue();
        } catch (NullPointerException e) {
            return null;                   //
        }
    }

    /**
     * @param cacheName
     * @param key
     * @param value
     */
    public void update(String cacheName, final Object key, final Object value) {
        this.put(cacheName, key, value);
    }

    /**
     * @param cacheName
     * @param key
     * @param value
     */
    public void put(String cacheName, final Object key, final Object value) {
        Cache cache = this.cacheManager.getCache(cacheName);
        if (cache == null) {
            throw new RuntimeException("please check ehcache.xml,there's not " +
                    cacheName + " cache partition");
        }
        cache.put(new Element(key, value));
    }

    /**
     * remove key-value
     *
     * @param cacheName
     * @param key
     */
    public void remove(String cacheName, final Object key) {
        Cache cache = this.cacheManager.getCache(cacheName);
        if (cache == null) {
            throw new RuntimeException("please check ehcache.xml,there's not " +
                    cacheName + " cache partition");
        }
        cache.remove(key);
    }


    /**
     * Removes all cached items.
     *
     * @param cacheName
     */
    public void clear(String cacheName) {
        Cache cache = this.cacheManager.getCache(cacheName);
        if (cache == null) {
            throw new RuntimeException("please check ehcache.xml,there's not " +
                    cacheName + " cache partition");
        }
        cache.removeAll();
    }


    /**
     * remove   cacheName partition
     *
     * @param cacheName
     */
    public void removeCache(String cacheName) {
        this.cacheManager.removeCache(cacheName);
    }


    @Deprecated
    private static class SingletonHolder {
        private static final EhcacheManager _instance = new EhcacheManager();
    }


}
