/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package testredis;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;

/**
 * @author : 石头哥哥
 *         Project : LandlordsServer1.0
 *         Date: 13-6-23
 *         Time: 下午7:24
 *         Connect: 13638363871@163.com
 *         packageName: PACKAGE_NAME
 */
@Service
public class testRedis {

    @Resource
    private RedisTemplate<Serializable, Serializable> redisTemplate;


    /**
     * 向redis里面添加key-value格式的数据
     *
     * @param key   key
     * @param value value
     */
    public void set(final Serializable key, final Serializable value) {
        redisTemplate.execute(new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
//                byte[] key_ = RedisUtil.getBytesFromObject(key);
//                byte[] value_ = RedisUtil.getBytesFromObject(value);
//                connection.set(key_, value_);
                return true;
            }
        });
    }

    /**
     * 根据key从redis里面取出value
     * @param key   key
     */
    public Serializable get(final Serializable key) {
        return redisTemplate.execute(new RedisCallback<Serializable>() {
            @Override
            public Serializable doInRedis(RedisConnection connection) throws DataAccessException {
//                byte[] keyBytes = RedisUtil.getBytesFromObject(key);
//                byte[] bytes = connection.get(keyBytes);
                return null;
            }
        });
    }


    public static void main(String[] args) throws Exception {


    }

}
