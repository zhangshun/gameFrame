/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

import com.dc.gameserver.baseConfig.Config;
import com.cache.SpringJredisCache.JRedisCache;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 石头哥哥</br>
 *         Date:1/9/14</br>
 *         Time:9:03 PM</br>
 *         Package:PACKAGE_NAME</br>
 *         Comment：
 */
@Service("testJredis")
@SuppressWarnings("unchecked")
public class TestJredis {
    @Resource
    private JRedisCache jRedisCache;



    public static void main(String[] args) {
        DOMConfigurator.configure(Config.DEFAULT_VALUE.FILE_PATH.LOG4J);

        System.setProperty("java.net.preferIPv4Stack", "true"); //Disable IPv6 in JVM
        /**初始化spring容器*/
//        BeanFactory springContext = new FileSystemXmlApplicationContext(Config.DEFAULT_VALUE.FILE_PATH.SPRING_CONFIG_PATH);
//        TestJredis testJredis = (TestJredis) springContext.getBean("testJredis");
//            ArrayList<Integer> list= new ArrayList<Integer>();
//            list.add(1);
//            list.add(2);
//            list.add(3);
//        testJredis.modelCacheService.addListByPrefixID("1",list);
//        ArrayList<Integer>  cacheList= (ArrayList<Integer>) testJredis.modelCacheService.findListByPrefixID("1");
//            testJredis.jRedisCache.putList("1",list);
//            ArrayList<Integer>  cacheList= (ArrayList<Integer>) testJredis.jRedisCache.getList("1");
//        for (int value:cacheList){
//            System.out.println(value);
//        }
        int[] tt=new int[3] ;
        tt[0]=1;
        tt[1]=2;
        tt[2]=3;
        System.out.println(tt[-1]);

    }

}
