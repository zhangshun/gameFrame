import com.alibaba.fastjson.JSON;

public static final Object parse(String text); // 把JSON文本parse为JSONObject或者JSONArray
public static final JSONObject parseObject(String text)； // 把JSON文本parse成JSONObject
public static final <T> T parseObject(String text, Class<T> clazz); // 把JSON文本parse为JavaBean
public static final JSONArray parseArray(String text); // 把JSON文本parse成JSONArray
public static final <T> List<T> parseArray(String text, Class<T> clazz); //把JSON文本parse成JavaBean集合
public static final String toJSONString(Object object); // 将JavaBean序列化为JSON文本
public static final String toJSONString(Object object, boolean prettyFormat); // 将JavaBean序列化为带格式的JSON文本
public static final Object toJSON(Object javaObject); 将JavaBean转换为JSONObject或者JSONArray。

1、替换其他所有的json库，java世界里没有其他的json库能够和fastjson可相比了。
2、使用fastjson的序列化和反序列化替换java serialize，java serialize不单性能慢，而且体制大。
3、使用fastjson替换hessian，json协议不必hessian体积大，而且fastjson性能优越，数倍于hessian
4、把fastjson用于memached缓存对象数据。
JavaBean、Collection、Map、Date、Enum、泛型