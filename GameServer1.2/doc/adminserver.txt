http操作工具：http://jodd.org/doc/http.html

1.获取服务器当前时间
GET http://localhost:8080/admin/serverTime
请求：无
响应：
{"data":{"time":1385912973000},"code":0,"message":"OK"}

2. 用户注册
POST http://localhost:8080/admin/users
请求:
  loginName: String
  loginPass: String
  email:String, 可选
  nickName, String 可选
  sex: F or M, 可选
响应：
{"data":{"id":49},"code":0,"message":"OK"}

3. 用户基本信息
GET /admin/users/$userId
请求参数：用户ID作为路径参数
响应：
{"data":{"id":49,"loginName":"xxx","email":"xxxx","nickName":"xxx","sex":"xxx"},"code":0,"message":"OK"}

4. 已登录用户查询自己的基本信息:
GET /admin/users/me
请求：无
响应：
{"data":{"id":49,"loginName":"xxx","email":"xxxx","nickName":"xxx","sex":"xxx"},"code":0,"message":"OK"}


5. 根据token查询用户信息
给游戏后台调用，会验证来源IP
GET /admin/users/
请求：token=xxx
响应：{"data":{"id":49,"loginName":"xxx","email":"xxxx","nickName":"xxx","sex":"xxx"},"code":0,"message":"OK"}


6. 用户名登录
POST: /admin/users/authentication
请求：loginName: {String}
     loginPass: {String}
响应：
{"data":{"token":"xxxxx"},"code":0,"message":"OK"}

7. token登录
POST: /admin/users/authentication/token/
请求：
token=xxxx
响应：
{"data":{"token":"xxxxx"},"code":0,"message":"OK"}
1. 去admin server登录，返回token，客户端加密保存这个token
2. 游戏后台以token作为参数，可以查询到玩家的信息，
3. 自动登录功能，可以用token作为参数去admin server登录
